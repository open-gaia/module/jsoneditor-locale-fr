# jsoneditor-locale-fr

[JSON Editor](https://github.com/json-editor/json-editor) french translations

## Installation

```javascript
import { JSONEditor } from "@json-editor/json-editor/src/core";
import LocaleFrTranslations from "@gaia/jsoneditor-locale-fr";

JSONEditor.defaults.languages["fr"] = LocaleFrTranslations;
JSONEditor.defaults.language = "fr";
```
